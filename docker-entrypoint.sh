#!/usr/bin/env bash

# check if the first argument passed in looks like a flag
if [ "$(printf %c "$1")" = '-' ]; then
  set -- /sbin/tini -- gulp "$@"
# check if the first argument passed in is gulp
elif [ "$1" = 'gulp' ]; then
  set -- /sbin/tini -- "$@"
else
  set -- /sbin/tini -- gulp "$@"
fi

exec "$@"